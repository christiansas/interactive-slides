<?php
//Is shown if a prof is logged in
//Displays all Folders in a given Presentations directory and displays radio buttons to select the presentation

if ($_SESSION['login_type'] != 'Dozent'){
        header("Location: ../index.php");
    }
?>

<html>
<head>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Interaktive Folienpraesentation Version</title>
</head>
  <body>
    <main>
      <div class="wrapper-main">
      <section class="section-default">
      <h1>Auswahl des Ordners</h1>
      <form class="form-signup" action="Includes/selecting_folder.inc.php" method="POST">
      <?php
      $directory = './Presentations/';
      $FolderNames = scandir($directory);
      foreach ($FolderNames as $FolderName) {
        if ($FolderName != "." && $FolderName != ".."){
        if (is_dir('./Presentations/'.$FolderName)) { ?>
      <label><input type='radio' name='FolderName' value= '<?php echo $FolderName; ?>' required> <?php echo $FolderName; ?> </label> <br> <?php }}} ?>
      </br></br>
      <button type="submit" name="Submit">Auswählen</button></br>
      </form>
      </section>
      </div>
      </br>
      <a href="Includes/logout_prof.inc.php">ausloggen</a>
    </main>
  </body>
</html>