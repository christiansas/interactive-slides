<?php
//Checks if user is Prof or Student and initialized sessions

require "../Classes/User.class.php";
$db = new User();

if (isset($_POST['username'])){
    $usertyp = $db -> getusertyp($_POST['username']);

    session_start();
    $_SESSION['username'] = $_POST['username'];
    $_SESSION['login_type'] = $usertyp[0]["Typ"];

    if ($_SESSION['login_type'] == 'Dozent' || $_SESSION['login_type'] == 'Student'){
    header("Location: ../index.php?success=selection");
    exit();
    } else {
          header("Location: ../initialize_session.php");
          exit();
      }
} else {
    header("Location: ../initialize_session.php");
    exit();
}
?>
