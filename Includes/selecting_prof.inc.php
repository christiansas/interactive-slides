<?php
//Set the session variable ProfName for the chosen prof

session_start();
if ($_SESSION['login_type'] != 'Student'){
        header("Location: ../index.php");
    }

if (isset($_POST['ProfName'])) {
  $profname = $_POST['ProfName'];
  $_SESSION['ProfName'] = $profname;
} else {header("Location: ../index.php"); }

header("Location: ../PresentationStudent.php");
?>