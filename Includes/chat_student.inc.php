<?php
//Get the chat message from the student via POST and write it in the chat-log
    session_start();
    require "../Classes/Chat.class.php";
    $db = new Chat();  

    $selection = htmlspecialchars(stripslashes(trim($_POST['selection'])));
    
    if(isset($_SESSION['username']) && $_SESSION['login_type'] == 'Student' && $selection == 'newmessage'){

        $usrmessage = htmlspecialchars(stripslashes(trim($_POST['usrmessage'])));
        $usrmessage_anonymous = $_POST['usrmessage_anonymous'];
        $sessionvar_username = $_SESSION['username'];

        if ($usrmessage_anonymous == 'true'){
            $current_folderpath = $db -> getCurrentFolderPath();
            $db -> insertchatmessage('Anonym', $usrmessage, "true", $current_folderpath[0]['folderpath']);
            $alertmessage = 'Die Nachricht wurde anonym versendet.';
        }
        elseif($usrmessage_anonymous == 'false'){
            $current_folderpath = $db -> getCurrentFolderPath();
            $db -> insertchatmessage($sessionvar_username, $usrmessage, "true", $current_folderpath[0]['folderpath']);
            $alertmessage = 'Die Nachricht wurde als '.$sessionvar_username.' versendet.';
        }
        
        echo json_encode(
            array(
                'alertmessage' => $alertmessage
            )
        );
    }
    elseif(isset($_SESSION['username']) && $_SESSION['login_type'] == 'Student' && $selection == 'interval'){
        $state_anonymous = $db -> getStateAnonymous($_SESSION['ProfName']);

        echo json_encode($state_anonymous);
    }
    elseif(isset($_SESSION['username']) && $_SESSION['login_type'] == 'Student' && $selection == 'checksessiondia'){
        $session_state_dia = $db -> checkifSessionIsInitiated();

        echo json_encode($session_state_dia);
    }
    else{
        header("Location: ../chat_student.php");
        exit();
    }

?>