<?php
//Actions of the side change buttons from the prof and updating the database with the new page

session_start();
require "../Classes/Page.class.php";
$db = new Page();

switch ($_POST["selectedButton"]) {
    case "Previous":
        if($_SESSION['PageID'] > 0) {
        $_SESSION['PageID']--;
        }
        break;
    case "Next":
        if($_SESSION['PageID'] <= $_SESSION['NumberOfPages'] - 2) {
        $_SESSION['PageID']++;
        }
        break;
    case "First":
        if($_SESSION['PageID'] != 0) {
        $_SESSION['PageID'] = 0;
        }
        break;
    case "Last":
        if($_SESSION['PageID'] != $_SESSION['NumberOfPages'] - 1) {
        $_SESSION['PageID'] = $_SESSION['NumberOfPages'] - 1;
        }
        break;
}

$db -> updatecurrentpage($_SESSION['username'], $_SESSION['path'].$_SESSION['pages'][$_SESSION['PageID']], $_SESSION['path'], $_SESSION['PageID']);
?>