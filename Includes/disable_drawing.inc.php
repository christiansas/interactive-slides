<?php
session_start();
require "../Classes/Page.class.php";
$db = new Page();

//Checks if the start drawing button should be enabled (green) or disabled (red)
$ToolSwitch = $db -> getToolSwitch($_SESSION['ProfName']);
if($ToolSwitch[0]["ToolsActive"] == 0){
    $Drawer = $db -> getDrawer($_SESSION['ProfName']);
    if (empty($Drawer)){
    echo "false";
    } else if (empty($Drawer[0]["isDrawing"])){
        echo "false";
    } else if ($Drawer[0]["isDrawing"] !== $_SESSION['username']) {
        echo "true";
    } else if ($Drawer[0]["isDrawing"] == $_SESSION['username']) {
        echo "false";
    }
} else if ($_SESSION['login_type'] != 'Dozent'){
    echo "true";
} else if ($_SESSION['login_type'] == 'Dozent'){
    echo "false";
}
?>