<?php
//Save the current canvas into the canvas folder of the current presenation

session_start();
require "../Classes/Page.class.php";
$db = new Page();

    // get the dataURL
    $dataURL = $_POST["canvas"];

    // the dataURL has a prefix (mimetype+datatype) 
    // that we don't want, so strip that prefix off
    $parts = explode(',', $dataURL);  
    $data = $parts[1];

    // Decode base64 data, resulting in an image
    $data1 = base64_decode($data);

    //Definition of the folderpath of the presentation + adding the "Canvas" Folder
    if($_SESSION['login_type'] == 'Student'){
        $folderpathandpageID = $db -> getcurrentfolderpathandpageID($_SESSION['ProfName']);
        $folder = "../" . $folderpathandpageID[0]['folderpath'];
        $path = $folder . "Canvas/";
    } else{
        $path = $_SESSION['pathforincludes'] . "Canvas/";
    }

    //If in the current Presentation folder is no Folder with Canvas create this Folder
    if (!is_dir($path)) {
        mkdir($path);
    }

    //Get the current page number if Student
    if ($_SESSION['login_type'] == 'Student') {
        $folderpathandpageID = $db -> getcurrentfolderpathandpageID($_SESSION['ProfName']);
        $Page = $folderpathandpageID[0]['pageID'];
        $pageID = $Page + 1;
    }

    //Get the current page number if Prof
    if ($_SESSION['login_type'] == 'Dozent') {
    $pageID = $_SESSION['PageID'] + 1;
    }

    // write the file to the upload directory
    file_put_contents($path . "Canvas " . $pageID .'.png', $data1);

    $ChangeTime = microtime();

    //Set ChangeTime to avoid Cache problems
    if (!empty($_SESSION['ProfName'])) {
        $db -> setChangeTime($_SESSION['ProfName'], $ChangeTime);
    } else {
        $db -> setChangeTime($_SESSION['username'], $ChangeTime);
    }

?>

