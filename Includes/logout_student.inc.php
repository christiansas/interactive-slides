<?php
//Logout the student + solving the problem if a drawing student ist logging out

session_start();
require "../Classes/Page.class.php";
$db = new Page();

$Drawer = $db -> getDrawer($_SESSION['ProfName']);
if ($Drawer[0]["isDrawing"] == $_SESSION['username']) {
    $db -> stopDrawing($_SESSION['ProfName']);
}

if($_SESSION['ProfMissing'] == 'ProfMissing') {
    header("Location: ../index.php");
} else {
    session_destroy();
    header("Location: ../index.php");
    }
?>