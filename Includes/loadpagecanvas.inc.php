<?php
session_start();
require "../Classes/Page.class.php";
$db = new Page();

//Get the folderpath of the current page and get the changetime to avoid browser cache issues
if (!empty($_SESSION['ProfName'])) {
    $folderpathandpageID = $db -> getcurrentfolderpathandpageID($_SESSION['ProfName']);
    $ChangeTime = $db -> getChangeTime($_SESSION['ProfName']);
} else {
    $folderpathandpageID = $db -> getcurrentfolderpathandpageID($_SESSION['username']);
    $ChangeTime = $db -> getChangeTime($_SESSION['username']);
}

//Display the canvas of the current page + if is needed to avoid problems if there is no canvas
if (!empty($folderpathandpageID[0]["folderpath"])) {
    $folderpath = "./" . $folderpathandpageID[0]["folderpath"] . "Canvas/";
    $pageID = $folderpathandpageID[0]["pageID"] + 1;

    $file = $folderpath . "Canvas " . $pageID . ".png";

    if (!is_file("." . $file)){
        echo "";
    } else {
        echo '<img src="'.$file. "?=" . $ChangeTime[0]["ChangeTime"] . '" id="PageCanvas">';
    }
} else {
    //Display no canvas
    echo '<img src=" ">';
}
?>

