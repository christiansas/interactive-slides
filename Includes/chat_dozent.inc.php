<?php
//Get the chat messages that were sent by student via POST
    session_start();
    require "../Classes/Chat.class.php";
    $db = new Chat();  
    
    $selection = htmlspecialchars(stripslashes(trim($_POST['selection'])));
    //$selection = 'interval';

    if(isset($_SESSION['username']) && $_SESSION['login_type'] == 'Dozent' && $selection == 'interval'){
        $messages = array();
        $current_folderpath = $db -> getCurrentFolderPath();
        $messages = $db -> getchatmessages($current_folderpath[0]['folderpath']);

        if ($_POST['enable_anym'] == "true") {
            $EnableAnonymous = 0;
        } else if ($_POST['enable_anym'] == "false") {
            $EnableAnonymous = 1;
        }

        $db -> changeStateAnonymous($EnableAnonymous, $_SESSION['username']);
        
        echo json_encode($messages);
    }

    elseif(isset($_SESSION['username']) && $_SESSION['login_type'] == 'Dozent' && $selection == 'updateMessages'){
        $db -> updateMessages(htmlspecialchars(stripslashes(trim($_POST['new_message_identifier']))));
    }
    
    else{
        header("Location: ../chat_dozent.php");
        exit();
    }
?>