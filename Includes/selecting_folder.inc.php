<?php
//Safe the selection of the folder from the radio buttons through the prof and safe the page paths in a session variable

session_start();

require "../Classes/Page.class.php";
$db = new Page();

if ($_SESSION['login_type'] != 'Dozent'){
        header("Location: ../index.php");
    }

$_SESSION['ProfName'] = $_SESSION['username'];
$_SESSION['canvasID'] = 0;

if (isset($_POST['FolderName'])) {
  $FolderName = $_POST['FolderName'];
}

//Define the path of the folder of the current presentatiom
$_SESSION['pathforincludes'] = "../Presentations/" .$FolderName.'/';
$_SESSION['path'] = substr($_SESSION['pathforincludes'], 3);

//Safe the path of the pages (PNG) of the folder in a session variable and sort them
$files = scandir($_SESSION['pathforincludes']);
foreach ($files as $value) {
    $fileType = mime_content_type($_SESSION['pathforincludes'].$value);
    if ($fileType == 'image/png')  {
        $pages[] = $value;
        }
    }
array_multisort($pages,SORT_ASC,SORT_NATURAL);
$_SESSION['pages'] = $pages;
$_SESSION['PageID'] = 0;
$CurrentPageName = $_SESSION['pages'][$_SESSION['PageID']];

//Insert the current (first) page into the database
$db -> insertcurrentpage($_SESSION['username'], $_SESSION['path'].$CurrentPageName, $_SESSION['path'], $_SESSION['PageID']);

header("Location: ../PresentationProf.php");
?>