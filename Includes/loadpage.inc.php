<?php
session_start();
require "../Classes/Page.class.php";
$db = new Page();

//Get the folderpath of current page of the presentation
if (isset($_SESSION['ProfName'])) {
    $filepath = $db -> getcurrentpage($_SESSION['ProfName']);
    } else {
    $filepath = $db -> getcurrentpage($_SESSION['username']);
    }
//Log out if prof is missing otherwise display the current page
if (empty($filepath) && $_SESSION['login_type'] != 'Dozent'){
    $_SESSION['ProfMissing'] = 'ProfMissing';
    echo 'ProfMissing';
} else {
    $_SESSION['PageImageSource'] = $filepath[0]["filepath"];
    echo '<img id="PageImgStudent" src="'.$_SESSION['PageImageSource'].'" class="PageImg">';
}
?>