<?php
//Logout the prof, destroy his session and delete the session in the database

session_start();
require "../Classes/Page.class.php";

$db = new Page();
$db -> deletePageSession($_SESSION['username']);

session_destroy();
header("Location: ../index.php");
?>