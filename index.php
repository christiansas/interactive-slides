<html>
<head>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Interaktive Folienpraesentation Version</title>
</head>
<main>
<?php
    session_start();

    //Show Page for logged-in lecturer
    if (isset($_SESSION['username']) && $_SESSION['login_type'] == 'Dozent'){
        require 'logged_in_dozent.php';
    }
    //Show Page for logged-in student
    elseif (isset($_SESSION['username']) && $_SESSION['login_type'] == 'Student'){
        require 'logged_in_student.php';
    }
    else{
    //Noone is logged in
    echo '
        <a href="initialize_session.php">Start Session</a>
        <h1>Herzlich Willkommen</h1>
        <h2>Interaktive Folienpraesentation Version <i>1.0</i></h2>
    ';
    }
?>
</main>
</html>
