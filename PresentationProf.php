<link href="design.css" rel="stylesheet">

<?php
session_start();
require "Classes/Page.class.php";
$db = new Page();

if ($_SESSION['login_type'] != 'Dozent'){
        header("Location: ../index.php");
    }

$_SESSION['NumberOfPages'] = count($_SESSION['pages']);
?>

<!--
    Checks if the drawing button is disabled or enabled
-->
<script>
    function disable_drawing() {
                var xmlhttp;
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    console.log(xmlhttp.response);
                    if (xmlhttp.response == "false"){
                        document.getElementById("draw_button").disabled = false;
                        if(document.getElementById("draw_button").style.background=='grey'){
                            document.getElementById("draw_button").style.background='green';
                            }
                        }
                    if (xmlhttp.response == "true"){
                        document.getElementById("draw_button").disabled = true;
                        document.getElementById("draw_button").style.background='grey';
                    }
                    }
                };
                xmlhttp.open("GET", "Includes/disable_drawing.inc.php", true);
                xmlhttp.send();
        }
        setInterval(function(){
           disable_drawing();
        }, 100);
</script>

<!DOCTYPE html>
<html>
<body>
<main>
    <div id= "Page">
    <div id= "PageImage">
<!--
    Displays the current page selected by the user
-->
    <script>
    function refresh_logs() {
        var xmlhttp;
        xmlhttp= new XMLHttpRequest();
        xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
        document.getElementById("PageImage").innerHTML=xmlhttp.responseText;
    }

        };
        xmlhttp.open("GET", "Includes/loadpage.inc.php", true);
        xmlhttp.send();
    }
    setInterval(function(){
        refresh_logs();
    }, 100);
    </script>

    </div>

        <div id= "PageCanvasSaving">

        <!--
            Displays the current image of the canvas
        -->
        <script>
        function refresh_canvassaving() {
            if (draw_iterator !== 1) {
                var xmlhttp;
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    document.getElementById("PageCanvasSaving").innerHTML = xmlhttp.responseText;
                    console.log(xmlhttp.responseText);
                    }
                };
                xmlhttp.open("GET", "Includes/loadpagecanvas.inc.php", true);
                xmlhttp.send();
            } else {
                document.getElementById("PageCanvasSaving").innerHTML = '<img src=" ">';
            }
        }
        setInterval(function(){
           refresh_canvassaving();
        }, 100);
        </script>
        </div>
        </div>

        <?php
        //Creates a canvas in the same size as the pages of the presentation
        list($width, $height, $type, $attr) = getimagesize($_SESSION['path'].$_SESSION['pages'][$_SESSION['PageID']]);
        echo '<canvas id="canvas" width="'.$width.'" height="'.$height.'"></canvas>';
        ?>

    </div>


    <!--
        Displays all Buttons of the Prof
    -->
    <div id="FunctionAndChat">
    <div class="FunctionBar">
                      <a href="Images/Bedienungsanleitung_Dozent.pdf" target="_blank">
                        <img alt="Qries" src="Images/icons8-hilfe-48.png" width="30" height="30" id="help_button">
                      </a>
                      <a href="Includes/logout_prof.inc.php">
                        <img alt="Qries" src="Images/exit.png" width="30" height="30" id="logout_button">
                      </a>
                      <br>
                      <input type="image" id="First" src="Images/erste.png" onClick="switchPage(this)">
                      <input type="image" id="Previous" src="Images/links.png" onClick="switchPage(this)">
                      <input type="image" id="Next" src="Images/rechts.png" onClick="switchPage(this)">
                      <input type="image" id="Last" src="Images/letzte.png" onClick="switchPage(this)">
                      <br>
                      <br>
    <div class="tools">
                <input class="color-field" id="black" style="background: black;" onClick="color(this)">
                <input class="color-field" id="blue" style="background: blue;" onClick="color(this)">
                <input class="color-field" id="red" style="background: red;" onClick="color(this)">
                <input class="color-field" id="green" style="background: green;" onClick="color(this)">
                <input class="color-field" id="yellow" style="background: yellow;" onClick="color(this)">
                <input class="color-field" id="hotpink" style="background: hotpink;" onClick="color(this)">
                <br>
                <br>
                <input id="writing_button" class="text" type="image" src="Images/icons8-text-96.png" onClick="isWriting()">
                <input id="erase_button" class="eraser" type="image" src="Images/icons8-radiergummi-96.png" onClick="erase()">
                <input type="range" min="1" max="20" class="pen-range" value= "3" onInput="draw_width = this.value">
                <input class="delete-all" type="image" src="Images/icons8-müll-52.png" onClick="deleteAll()">
                <br>
                <br>
    </div>
        <input id="draw_button" type="Button" onClick="isDrawing()" style="background: green;" value="Zeichnen beginnen">

        <input id="switchTools" type="Button" onClick="switchTools()" style="background: green;" value="Tools aktiv">
    </div>

    <div class="ChatFunctionProf">
            <div id="chat_dozent">
                <div id="chat_dozent_header_container">
                    <p id="chat_dozent_description">Nachrichten: <p>
                </div>
                <div id="chat_dozent_chatbox">
                </div>
            </div>

            <div id="chat_dozent_optionen">
                </br>
                <input type="checkbox" id="enable_anonymous" checked>
                <label for="enable_anonymous" id="enable_anonymous_label">Anonym senden erlauben</label>
            </div>

            <script>
                //Interval for sending the state of the anonymous button to database
                //Response in responseObject holds every chat message in the database
                //for further information check the operations in handleResponse
                setInterval(() => {
                    const request = new XMLHttpRequest();

                    request.onload = () => {
                        console.log(request.responseText);
                        let responseObject = null;

                        try{
                            responseObject = JSON.parse(request.responseText);
                            console.log("parsed");
                        } catch (e) {
                            alert(e);
                        }

                        if(responseObject){
                            handleResponse(responseObject);
                        }
                    }

                    //Check for anonymous enable
                    var enable_anym = document.getElementById("enable_anonymous").checked;

                    const requestData = `selection=interval&&enable_anym=${enable_anym}`;

                    request.open("POST", "Includes/chat_dozent.inc.php");
                    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    request.send(requestData);
                }, 500);

                //Interval for sending the state of the tools button to database
                //There is no response because only the student has to check this value (enabled or not)
                setInterval(() => {
                    const request_tools = new XMLHttpRequest();

                    request_tools.onload = () => {
                        //Do nothing
                    }

                    var enable_tools = document.getElementById("enable_tools").checked;

                    const requestData_enable_tools = `selection=interval_tools&&enable_tools=${enable_tools}`;

                    request_tools.open("POST", "Includes/chat_dozent.inc.php");
                    request_tools.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    request_tools.send(requestData_enable_tools);
                }, 500);

                //handleResponse
                function handleResponse(responseObject){

                    var output;
                    var output_agg = null;
                    var new_message_alert = false;
                    var new_message;

                    //loop to go through all messages that the db holds
                    for(var i = 0; i < responseObject.length; i++) {
                        var obj = responseObject[i];

                        console.log(obj.username);
                        console.log(obj.message);
                        console.log(obj.new);

                        //this variable checks if theres a new message in the db (this is important for the sound)
                        if(obj.new == 'true'){
                            new_message_alert = true;
                            new_message = obj.id;
                        }

                        var username = obj.username;
                        var message = obj.message;

                        //Build the output
                        output = '<b>' + username + ':' + '</b>' + ' ' + message + '</br>';
                        output_agg = output_agg + output;
                    }

                    //look up the description for updateMessages
                    if(new_message_alert == true){
                        updateMessages(new_message);
                    }

                    //Output the messages
                    output_agg_cut = output_agg.substring(4);
                    console.log(output_agg_cut);
                    document.getElementById('chat_dozent_chatbox').innerHTML = output_agg_cut;

                    //Plays a signal tone if a new message was present
                    if(new_message_alert == true){
                        play();
                        //Scrolls to bottom when new message arrives
                        var objDiv = document.getElementById("chat_dozent_chatbox");
                        objDiv.scrollTop = objDiv.scrollHeight;
                    }

                }

                //if there was a new message (each new message has a specific column)
                //this function sets the column of the specific message row back to a not new message
                function updateMessages(new_message) {
                    const request = new XMLHttpRequest();

                    request.onload = () => {
                        //Do Nothing
                    }

                    const requestData = `selection=updateMessages&&new_message_identifier=${new_message}`;

                    request.open("POST", "Includes/chat_dozent.inc.php");
                    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    request.send(requestData);
                }

                //Signal sound for a new message
                function play() {
                    var audio = new Audio('Audio/pristine-609.mp3');
                    audio.play();
                }
            </script>
    </div>
    </div>

</main>
</body>
</html>
<script src="canvas.js"></script>