<?php
//Is shown if a student is logged in
//Displays all profs from the database and displays radio buttons to select the prof
//Error message if the prof isn't logged in

if ($_SESSION['login_type'] != 'Student'){
        header("Location: ../index.php");
    }
require "Classes/User.class.php";
$db = new User();
?>

<html>
<head>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Interaktive Folienpraesentation Version</title>
</head>
  <body>
    <main>
      <div class="wrapper-main">
      <section class="section-default">
      <h1>Auswahl der Vorlesung bzw. des Dozenten</h1>
      <form class="form-signup" action="Includes/selecting_prof.inc.php" method="POST">
      <?php

      $profnames = $db -> getprof();
      foreach ($profnames as $profname) { ?>
      <label><input type='radio' name='ProfName' value= "<?php echo $profname["username"]; ?> "required> <?php echo $profname["username"]; ?> </label> <br> <?php } ?>
      </br></br>
      <button type="submit" name="Submit">Auswählen</button></br>
      </form>
      </section>
      </div>
      <h3><?php if (!empty($_SESSION['ProfMissing'])) { echo 'Es ist noch kein Dozent im Raum oder der Dozent hat den Raum verlassen. Bitte versuche es später noch einmal!';}?></h3>
      <a href="Includes/logout_student_indexpage.inc.php">ausloggen</a>
    </main>
  </body>
</html>

