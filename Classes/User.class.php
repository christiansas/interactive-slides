<?php
//All functions relevant to get or insert informations about the current user using database operations

require_once "Dbc.class.php";

class User extends Dbc {
        public function getprof(){
                $sql = 'SELECT username from user where typ = "Dozent";';
                       $dbc = $this->connection();
                       $stmt = $dbc->prepare($sql);
                       $stmt->execute();
                       $sqlReturn = $stmt->fetchAll();
                       return $sqlReturn;
        }
        public function getusername(){
                        $sql = 'SELECT username from user;';
                               $dbc = $this->connection();
                               $stmt = $dbc->prepare($sql);
                               $stmt->execute();
                               $sqlReturn = $stmt->fetchAll();
                               return $sqlReturn;
                }
        public function getusertyp($username){
                                $sql = 'SELECT Typ from user where username = (?);';
                                       $dbc = $this->connection();
                                       $stmt = $dbc->prepare($sql);
                                       $stmt->execute([$username]);
                                       $sqlReturn = $stmt->fetchAll();
                                       return $sqlReturn;
                        }
}
?>