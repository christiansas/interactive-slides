<?php
//All functions relevant for chat using database operations

require_once "Dbc.class.php";

class Chat extends Dbc {
    public function getchatmessages($current_folderpath){
        $sql = "SELECT id, username, message, new FROM chat WHERE folienset = (?);";
        $dbc = $this->connection();
        $stmt = $dbc->prepare($sql);
        $stmt->execute([$current_folderpath]);
        $sqlReturn = $stmt->fetchAll();
        return $sqlReturn;
    }
    public function insertchatmessage($username, $message, $new, $folienset){
        $sql = "INSERT INTO chat (username, message, new, folienset) VALUES (?,?,?,?);";
        $dbc = $this->connection();
        $stmt = $dbc->prepare($sql);
        $stmt->execute([$username, $message, $new, $folienset]);
    }
    public function updateMessages($new_message_identifier){
        $sql = "UPDATE chat SET new = 'false' where id = (?);";
        $dbc = $this->connection();
        $stmt = $dbc->prepare($sql);
        $stmt->execute([$new_message_identifier]);
    }
    public function changeStateAnonymous($enable_anym, $dozentname){
        $sql = "UPDATE page SET EnableAnonymous = (?) WHERE dozentname = (?);";
        $dbc = $this->connection();
        $stmt = $dbc->prepare($sql);
        $stmt->execute([$enable_anym, $dozentname]);
    }
    public function getStateAnonymous($dozentname){
        $sql = "SELECT EnableAnonymous FROM page WHERE dozentname = (?);";
        $dbc = $this->connection();
        $stmt = $dbc->prepare($sql);
        $stmt->execute([$dozentname]);
        $sqlReturn = $stmt->fetchAll();
        return $sqlReturn;
    }
    public function checkifSessionIsInitiated(){
        $sql = "SELECT count(*) as folderpath_count from page;";
        $dbc = $this->connection();
        $stmt = $dbc->prepare($sql);
        $stmt->execute();
        $sqlReturn = $stmt->fetchAll();
        return $sqlReturn;
    }
    public function getCurrentFolderPath(){
        $sql = "SELECT folderpath from page;";
        $dbc = $this->connection();
        $stmt = $dbc->prepare($sql);
        $stmt->execute();
        $sqlReturn = $stmt->fetchAll();
        return $sqlReturn;
    }
}
?>