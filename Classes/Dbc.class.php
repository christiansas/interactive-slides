<?php
//Initialization of the database connection

class Dbc
{
  private $host = "localhost";

  private $user = "root";

  private $password = "";

  private $dbName = "interaktive_folienpraesentation";

  /**
   *
   *
   * fetch-mode: associative arrays
   * returns the database-connection if successful 
   * exception-message: "Database Connection Failure"
   * connection-type: PDO
   * @return void
   */

  public function connection(){
    $dsn = 'mysql:host='.$this->host.';dbname='.$this->dbName;
    try {
      $pdo = new PDO($dsn,$this->user,$this->password);
      $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
      return $pdo;
    } 
    catch (PDOException $e) {
      error_log($e -> getMessage()) ;
      exit("Database Connection Failure");
    }
  }
}