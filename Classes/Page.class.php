<?php
//All functions relevant for the draw function and to display the pages of the presentation using database operations

require_once "Dbc.class.php";

class Page extends Dbc {
        public function insertcurrentpage($dozentname, $filepath, $folderpath, $pageID){
                $sql = "INSERT INTO Page (dozentname, filepath, folderpath, pageID) VALUES (?,?,?,?);";
                $dbc = $this->connection();
                $stmt = $dbc->prepare($sql);
                $stmt->execute([$dozentname, $filepath, $folderpath, $pageID]);
        }

        public function updatecurrentpage($dozentname, $filepath, $folderpath, $pageID){
                        $sql = "UPDATE Page SET filepath = (?) , folderpath = (?), pageID = (?) where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$filepath, $folderpath, $pageID, $dozentname]);
                }

        public function getcurrentpage($dozentname){
                        $sql = "SELECT filepath from Page where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$dozentname]);
                        $sqlReturn = $stmt->fetchAll();
                        return $sqlReturn;
                }

        public function getcurrentfolderpathandpageID ($dozentname){
                        $sql = "SELECT folderpath, pageID from Page where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$dozentname]);
                        $sqlReturn = $stmt->fetchAll();
                        return $sqlReturn;
                    }

        public function deletePageSession($dozentname){
                        $sql = "DELETE FROM page where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$dozentname]);
                }

        public function isDrawing($dozentname, $username){
                        $sql = "UPDATE Page SET isDrawing = (?) where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$username, $dozentname]);
                        }

        public function stopDrawing($dozentname){
                        $sql = "UPDATE page SET isDrawing = NULL where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$dozentname]);
                        }

        public function getDrawer($dozentname){
                        $sql = "SELECT isDrawing FROM Page where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$dozentname]);
                        $sqlReturn = $stmt->fetchAll();
                        return $sqlReturn;
                        }

        public function getChangeTime($dozentname){
                        $sql = "SELECT ChangeTime FROM Page where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$dozentname]);
                        $sqlReturn = $stmt->fetchAll();
                        return $sqlReturn;
                        }

        public function setChangeTime($dozentname, $ChangeTime){
                        $sql = "UPDATE Page SET ChangeTime = (?) where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$ChangeTime, $dozentname]);
                        }

        public function enableTools($dozentname){
                        $sql = "UPDATE page SET ToolsActive = 0 where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$dozentname]);
                        }

        public function disableTools($dozentname){
                        $sql = "UPDATE page SET ToolsActive = 1 where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$dozentname]);
                        }

        public function getToolSwitch($dozentname){
                        $sql = "SELECT ToolsActive FROM Page where dozentname = (?);";
                        $dbc = $this->connection();
                        $stmt = $dbc->prepare($sql);
                        $stmt->execute([$dozentname]);
                        $sqlReturn = $stmt->fetchAll();
                        return $sqlReturn;
                        }
}
?>