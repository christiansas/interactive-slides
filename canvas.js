var canvas = document.getElementById("canvas");

const colorField = document.querySelector(".color-field");


let context = canvas.getContext("2d");

//Initialize all required variables
//Iterators are mainly used for double-used buttons
var draw_color; //Color of pencil and text
let draw_width = "3"; //Width of pencil and eraser, size of text
let is_drawing = false; //Currently drawing or not
var draw_iterator = 0; //Tools are activated or not
var switch_iterator = 0; //The tools activate button is activated or not
var drawing_iterator = 0; //Pencil and eraser are activated or not
var writing_iterator = 0; //Writing is activated or not
var erase_iterator = 0; //Erasing is activated or not
var mouseX = 0;
var mouseY = 0;
var startingX = 0;
var recentText = [];
var undoLetters = [];

canvas.addEventListener("touchstart", start, false);
canvas.addEventListener("touchmove", draw, false);
canvas.addEventListener("mousedown", start, false);
canvas.addEventListener("mousemove", draw, false);

canvas.addEventListener("touchend", stop, false);
canvas.addEventListener("mouseup", stop, false);
canvas.addEventListener("mouseout", stop, false);

//Show the currently used color with a Border so it appears thicker
function changeColor() {
    document.getElementById("blue").style.borderColor = '#e8e8e8';
    document.getElementById("red").style.borderColor = '#e8e8e8';
    document.getElementById("green").style.borderColor = '#e8e8e8';
    document.getElementById("yellow").style.borderColor = '#e8e8e8';
    document.getElementById("hotpink").style.borderColor = '#e8e8e8';
    document.getElementById("black").style.borderColor = '#e8e8e8';
}

//If currently eraser is selected change to drawing
//Select the current color and display it
function color(element) {
    if (erase_iterator == 1) {
        stopEraser();
        drawing_iterator = 1;
    }
    context.globalCompositeOperation = "source-over";
    changeColor();
    switch (element.id) {
        case "black":
            draw_color = "black";
            document.getElementById("black").style.borderColor = 'transparent';
            break;
        case "blue":
            draw_color = "blue";
            document.getElementById("blue").style.borderColor = 'transparent';
            break;
        case "red":
            draw_color = "red";
            document.getElementById("red").style.borderColor = 'transparent';
            break;
        case "green":
            draw_color = "green";
            document.getElementById("green").style.borderColor = 'transparent';
            break;
        case "yellow":
            draw_color = "yellow";
            document.getElementById("yellow").style.borderColor = 'transparent';
            break;
        case "hotpink":
            draw_color = "hotpink";
            document.getElementById("hotpink").style.borderColor = 'transparent';
            break;
    }
}

function stopEraser() {
    erase_iterator = 0;
    context.globalCompositeOperation = "source-over";
    draw_color = "black";
    document.getElementById("erase_button").style.borderColor = 'transparent';
    document.getElementById("black").style.borderColor = 'transparent';
}

function startEraser() {
    erase_iterator = 1;
    writing_iterator = 0;
    document.getElementById("writing_button").style.borderColor = 'transparent';
    document.getElementById("erase_button").style.borderColor = 'black';
    context.globalCompositeOperation = "destination-out";
    context.strokeStyle = "rgba(255,255,255,1)";
    changeColor();
}
function erase() {
    if (draw_iterator == 1) {
        if (erase_iterator == 0) {
            if (writing_iterator == 1) {
                stopWriting();
            }
            startEraser();
        } else if (erase_iterator == 1) {
            stopEraser();
            drawing_iterator = 1;
        }
    }
}

function deleteAll() {
    if (draw_iterator == 1) {
        context.clearRect(0, 0, canvas.width, canvas.height);
    }
}

function start(event) {
    if (drawing_iterator == 1) {
        is_drawing = true;
        context.beginPath();
        context.moveTo(event.clientX - canvas.offsetLeft, event.clientY - canvas.offsetTop)
        event.preventDefault();
    }
}

function draw(event) {
    if (is_drawing) {
        context.lineTo(event.clientX - canvas.offsetLeft, event.clientY - canvas.offsetTop);
        context.strokeStyle = draw_color;
        context.lineWidth = draw_width;
        context.lineCap = "round";
        context.lineJoin = "round";
        context.stroke();
    }
    event.preventDefault();
}

function stop(event) {
    if (is_drawing) {
        context.stroke();
        context.closePath();
        is_drawing = false;
    }
    event.preventDefault();
}

function saveText() {
    undoLetters.push(canvas.toDataURL());
}

saveText();

function undo() {
    undoLetters.pop();
    var imgData = undoLetters[undoLetters.length - 1];
    var image = new Image();
    image.src = imgData;
    image.onload = function() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(image, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
    }
}

addEventListener("click", function(e) {
    mouseX = e.pageX - canvas.offsetLeft;
    mouseY = e.pageY - canvas.offsetTop;
    startingX = mouseX;
    recentText = [];
    return false;
}, false);

document.addEventListener("keydown", function(e) {

    if (writing_iterator == 1) {
        canvas.style.font = canvas.getContext("2d").font;
        canvas.style.fontSize = 5 * draw_width + "px";
        context.fillStyle = draw_color;
        canvas.getContext("2d").font = canvas.style.font;

        //No written keynames
        if (e.keyCode === 17 || e.keyCode === 16 || e.keyCode === 20 || e.keyCode === 9 || e.keyCode === 27 || e.keyCode === 44 || e.keyCode === 45 || e.keyCode === 46 || e.keyCode === 38 || e.keyCode === 39 || e.keyCode === 40 || e.keyCode === 37 || e.keyCode === 18 || e.keyCode === 220 || e.keyCode === 221) {

            //backspace
        } else if (e.keyCode === 8) {
            undo();
            var recentWords = recentText[recentText.length - 1];
            mouseX -= context.measureText(recentWords).width
            recentText.pop();

            //enter
        } else if (e.keyCode === 13) {
            mouseX = startingX;
            mouseY += 5.5 * draw_width;

        } else {
            context.fillText(e.key, mouseX, mouseY);
            mouseX += context.measureText(e.key).width;
            saveText();
            recentText.push(e.key);
        }
    }
}, false);

function saveCanvas() {
    var dataURL = canvas.toDataURL();
    const request = new XMLHttpRequest();
    request.open("POST", "Includes/canvas_save.inc.php");
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.send("canvas=" + encodeURIComponent(dataURL));
}

function loadCanvas() {
    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    var img = document.getElementById("PageCanvas");
    if (img !== null) {
        ctx.drawImage(img, 0, 0);
    }
}

function disableDrawing() {
    document.getElementById("draw_button").style.background = 'green';
    document.getElementById("draw_button").value = 'Zeichnen beginnen';
}

function enableDrawing() {
    document.getElementById("draw_button").style.background = 'red';
    document.getElementById("draw_button").value = 'Zeichnen beenden';
}

function lockDrawing() {
    var request = new XMLHttpRequest();
    request.onload = () => {
        //Do Nothing
    }
    var requestData = '<?php $_SESSION["username"]?>';
    request.open("POST", "Includes/lock_drawing.inc.php");
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.send(requestData);
}

function unlockDrawing() {
    var request = new XMLHttpRequest();
    request.onload = () => {
        //Do Nothing
    }
    request.open("POST", "Includes/unlock_drawing.inc.php");
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.send();
}

function isDrawing() {
    if (draw_iterator == 1) {
        unlockDrawing();
        disableDrawing();
        saveCanvas();
        deleteAll();
        draw_iterator = 0;
        drawing_iterator = 0;
        if (writing_iterator == 1) {
            stopWriting();
        }
        if (erase_iterator == 1) {
            stopEraser();
        }
    } else if (draw_iterator == 0) {
        lockDrawing();
        enableDrawing();
        loadCanvas();
        draw_iterator = 1;
        drawing_iterator = 1;
        writing_iterator = 0;
    }
}

function startWriting() {
    writing_iterator = 1;
    drawing_iterator = 0;
    document.getElementById("writing_button").style.borderColor = 'black';
}

function stopWriting() {
    writing_iterator = 0;
    drawing_iterator = 1;
    document.getElementById("writing_button").style.borderColor = 'transparent';
}

function isWriting() {
    if (draw_iterator == 1) {
        if (writing_iterator == 1) {
            stopWriting();
        } else if (writing_iterator == 0) {
            if (erase_iterator == 1) {
                stopEraser();
            }
            startWriting()
        }
    }
}

function switchTools() {
    if (switch_iterator == 1) {
        document.getElementById("switchTools").style.background = 'red';
        document.getElementById("switchTools").value = 'Tools deaktiv';
        switch_iterator = 0;
        writing_iterator = 0;
        document.getElementById("writing_button").style.borderColor = 'transparent';
        document.getElementById("erase_button").style.borderColor = 'transparent';
        var request = new XMLHttpRequest();
        request.open("POST", "Includes/disable_tools.inc.php");
        request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        request.send();


    } else if (switch_iterator == 0) {
        document.getElementById("switchTools").style.background = 'green';
        document.getElementById("switchTools").value = 'Tools aktiv';
        switch_iterator = 1;
        var request = new XMLHttpRequest();
        request.open("POST", "Includes/enable_tools.inc.php");
        request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        request.send();

    }
}

function switchPage(element) {
    if (draw_iterator != 1 && document.getElementById("draw_button").disabled == false){
        var selectedButton = element.id;
        const request = new XMLHttpRequest();
        request.open("POST", "Includes/switch_page.inc.php");
        request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        request.send("selectedButton=" + selectedButton);
    }


}
