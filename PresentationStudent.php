<link href="design.css" rel="stylesheet">
<?php
//The main interface of the logged in student

session_start();
require "Classes/Page.class.php";
$db = new Page();

if ($_SESSION['login_type'] != 'Student'){
        header("Location: ../index.php");
    }
?>
<!--
    Checks if the drawing button is disabled or enabled
    If the student is drawing and the status of the button changes it saves the drawing
-->

<script>
    function disable_drawing() {
                var xmlhttp;
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange=function() {
                    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                        console.log(xmlhttp.response);
                        if (xmlhttp.response == "false"){
                            document.getElementById("draw_button").disabled = false;
                            if(document.getElementById("draw_button").style.background=='grey'){
                                document.getElementById("draw_button").style.background='green';
                                }
                            }
                        if (xmlhttp.response == "true"){
                            document.getElementById("draw_button").disabled = true;
                            document.getElementById("draw_button").style.background='grey';

                            if (draw_iterator == 1) {
                                unlockDrawing();
                                disableDrawing();
                                saveCanvas();
                                deleteAll();
                                draw_iterator = 0;
                                drawing_iterator = 0;
                                if (writing_iterator == 1) {
                                    stopWriting();
                                }
                                if (erase_iterator == 1) {
                                    stopEraser();
                                }
                            }
                        }
                    }
                };
                xmlhttp.open("GET", "Includes/disable_drawing.inc.php", true);
                xmlhttp.send();
        }
        setInterval(function(){
           disable_drawing();
        }, 100);
</script>

<!DOCTYPE html>
<html>
<body>
<main>
    <div id= "Page">
    <div id= "PageImage">

<!--
    Checks if the prof is missing
    Displays the current page shown by the prof
-->
    <script>
    function refresh_logs() {
        var xmlhttp;
        xmlhttp= new XMLHttpRequest();
        xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
        if(xmlhttp.responseText == 'ProfMissing'){
        window.location.href = 'Includes/logout_student.inc.php';
        }
        document.getElementById("PageImage").innerHTML=xmlhttp.responseText;

    }

        };
        xmlhttp.open("GET", "Includes/loadpage.inc.php", true);
        xmlhttp.send();
    }
    setInterval(function(){
        refresh_logs();
    }, 100);
    </script>

    </div>

    <div id= "PageCanvasSaving">

<!--
    Displays the current image of the canvas
-->
    <script>
    function refresh_canvassaving() {
            if (draw_iterator !== 1) {
                var xmlhttp;
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    document.getElementById("PageCanvasSaving").innerHTML = xmlhttp.responseText;
                    }
                };
                xmlhttp.open("GET", "Includes/loadpagecanvas.inc.php", true);
                xmlhttp.send();
            } else {
                document.getElementById("PageCanvasSaving").innerHTML = '<img src=" ">';
            }
        }
        setInterval(function(){
           refresh_canvassaving();
        }, 100);
    </script>

    </div>
        <?php
        //Creates a canvas in the same size as the pages of the presentation
        $filepath = $db -> getcurrentpage($_SESSION['ProfName']);
        if (!empty($filepath)){
        list($width, $height, $type, $attr) = getimagesize($filepath[0]["filepath"]);
        echo '<canvas id="canvas" width="'.$width.'" height="'.$height.'"></canvas>';
        }
        ?>
    </div>

    <!--
        Displays all Buttons of the Student
    -->
    <div id="FunctionAndChat">
    <div class="FunctionBar">
        <a href="Images/Bedienungsanleitung_Student.pdf" target="_blank">
        <img alt="Qries" src="Images/icons8-hilfe-48.png" width="30" height="30" id="help_button">
        </a>
        <a href="Includes/logout_student.inc.php">
        <img alt="Qries" src="Images/exit.png" width="35" height="35" id="logout_button">
        </a>
        <br><br><br>

        <div class="tools">
                <input class="color-field" id="black" style="background: black;" onClick="color(this)">
                <input class="color-field" id="blue" style="background: blue;" onClick="color(this)">
                <input class="color-field" id="red" style="background: red;" onClick="color(this)">
                <input class="color-field" id="green" style="background: green;" onClick="color(this)">
                <input class="color-field" id="yellow" style="background: yellow;" onClick="color(this)">
                <input class="color-field" id="hotpink" style="background: hotpink;" onClick="color(this)">
                <br><br>
                <input id="writing_button" class="text" type="image" src="Images/icons8-text-96.png" onClick="isWriting()">
                <input id="erase_button" class="eraser" type="image" src="Images/icons8-radiergummi-96.png" onClick="erase()">
                <input type="range" min="1" max="20" class="pen-range" value= "3" onInput="draw_width = this.value">
                <input class="delete-all" type="image" src="Images/icons8-müll-52.png" onClick="deleteAll()">
                <br><br>
                <input id="draw_button" type="Button" onClick="isDrawing()" style="background: green;" value="Zeichnen beginnen">
        </div>
    </nav>
    </div>
    <div class="ChatFunctionStudent">
        <?php
        $sessionvar_username = $_SESSION['username'];
        ?>
        <div id="chat_student">
            <div id="chat_student_header_container">
                <p id="chat_student_description">Nachricht senden: <p>
            </div>

            <div class="form">
                <textarea id="usrmessage" name="test"></textarea><br><br>
                <input type="checkbox" id="usrmessage_anonymous">
                <label for="usrmessage_anonymous" id="usrmessage_anonymous_label">Anonym senden</label>
                <button type="submit" id="student_chatbox_submit" onsubmit="validateTextArea()">Senden</button>
            </div>
        </div>

        <script>
                const form = {
                    usrmessage: document.getElementById('usrmessage'),
                    usrmessage_anonymous: document.getElementById('usrmessage_anonymous'),
                    submit: document.getElementById('student_chatbox_submit'),
                    message: document.getElementById('form-message')
                };

                //Event listener when a message should be sent
                //send the message and the information in the checkbox if it should be sent anonymously or not to the db
                form.submit.addEventListener('click', () => {
                    var checkifempty = document.getElementById('usrmessage').value;
                    //Check-Up so that no empty messages can be sent
                    if(checkifempty == null || checkifempty == ""){
                        alert("Leere Nachricht!");
                    }
                    else {
                    const request = new XMLHttpRequest();

                    request.onload = () => {
                        console.log(request.responseText);
                        let responseObject = null;

                        try{
                            responseObject = JSON.parse(request.responseText);
                            console.log("parsed");
                        } catch (e) {
                            alert(e);
                        }

                        if(responseObject){
                            handleResponse(responseObject);
                        }
                    };

                    const requestData = `usrmessage=${form.usrmessage.value}&usrmessage_anonymous=${form.usrmessage_anonymous.checked}&&selection=newmessage`;
                    console.log(requestData);

                    request.open('post', 'Includes/chat_student.inc.php');
                    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    request.send(requestData);
                    }
                });

                //handleResponse
                //plays a sound that the message has been sent + alert 
                //clears the chat area
                function handleResponse (responseObject) {
                    play();
                    alert(responseObject.alertmessage);
                    document.getElementById("usrmessage").value = "";
                }

                //this interval checks the state of the anonymous enable option wich is managed by the lecturer in the database 
                //if this is not enabled the student should not be able to send messages anonymously
                setInterval(() => {
                    const request_interval = new XMLHttpRequest();

                    request_interval.onload = () => {
                        console.log(request_interval.responseText);
                        let responseObject_interval = null;

                        try{
                            responseObject_interval = JSON.parse(request_interval.responseText);
                            console.log("parsed");
                        } catch (e) {
                            alert(e);
                        }

                        if(responseObject_interval){
                            handleResponse_interval(responseObject_interval);
                        }
                    }

                    const requestData_interval = `selection=interval`;

                    request_interval.open("POST", "Includes/chat_student.inc.php");
                    request_interval.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    request_interval.send(requestData_interval);
                }, 500);

                //handleResponse_interval 
                //Dis- or Enable the opportunity to send messages anonymously
                function handleResponse_interval(responseObject_interval) {

                    var obj = responseObject_interval[0];

                    if (obj.EnableAnonymous == 1){
                        document.getElementById("usrmessage_anonymous").disabled = true;
                        document.getElementById("usrmessage_anonymous").checked = false;
                        console.log("False state");
                    }
                    else if (obj.EnableAnonymous == 0){
                        document.getElementById("usrmessage_anonymous").disabled = false;
                        console.log("True state");

                    }
                }

                setInterval(() => {
                    const request_checkdia = new XMLHttpRequest();

                    request_checkdia.onload = () => {
                        console.log(request_checkdia.responseText);
                        let responseObject_checkdia = null;
                        try{

                            responseObject_checkdia = JSON.parse(request_checkdia.responseText);
                            console.log("parsed");
                            console.log(responseObject_checkdia[0].folderpath_count);
                        } catch (e) {
                            alert(e);
                        }
                        if(responseObject_checkdia){
                            handleResponse_dia(responseObject_checkdia);
                        }
                    }

                    const requestData_checkdia = `selection=checksessiondia`;

                    request_checkdia.open("POST", "Includes/chat_student.inc.php");
                    request_checkdia.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    request_checkdia.send(requestData_checkdia);
                }, 500);

                function handleResponse_dia(responseObject_checkdia){
                    var obj = responseObject_checkdia[0].folderpath_count;
                    
                    if(responseObject_checkdia[0].folderpath_count <= 0){
                        document.getElementById("usrmessage").disabled = true;
                        document.getElementById("student_chatbox_submit").disabled = true;
                        }
                    else {
                        document.getElementById("usrmessage").disabled = false;
                        document.getElementById("student_chatbox_submit").disabled = false;
                    }
                }

                function play() {
                    var audio = new Audio('Audio/done-for-you-612.mp3');
                    audio.play();
                }
        </script>
    </div>
    </div>

</main>
</body>
</html>
<script src="canvas.js"></script>